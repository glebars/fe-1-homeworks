let body = document.querySelector("body")
let burgerMenu = document.querySelector('.burger-menu__button')
let burgerMenuList = document.querySelector('.burger-menu__nav')
let burgerMenuLinks = document.querySelectorAll('.burger-menu__link')

function closeBurgerList() {
    burgerMenu.classList.remove("burger-menu_active")
    burgerMenuList.classList.remove('burger-menu__nav_active_display')
}

burgerMenu.addEventListener('click', (e) =>{
    e.preventDefault()
    e.stopPropagation()
    burgerMenu.classList.toggle("burger-menu_active")
    burgerMenuList.classList.toggle('burger-menu__nav_active_display')})

burgerMenuLinks.forEach((item =>{
    item.addEventListener('click', (e) => {
        e.preventDefault()
        closeBurgerList()
    })
}))

body.addEventListener('click', (e) =>{
    closeBurgerList()
})
