import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import ProductsList from "../ProductsList/ProductsList";

const Favourites = ({ addToMyBag, clothId, setClothId }) => {
  const cardsArr = useSelector((store) => store.app.cardsArr);
  const favorites = useSelector((store) => store.app.favorites);
  const addedToBag = useSelector((store) => store.app.addedToBag);

  useEffect(() => {
    localStorage.setItem("addedToBag", JSON.stringify(addedToBag));
    localStorage.setItem("favorites", JSON.stringify(favorites));
  });

  const favouriteCardsArr = [];

  const favorFunc = (item) => {
    favorites.forEach((code) => {
      if (item.code === code) {
        return favouriteCardsArr.push(item);
      }
    });
  };

  for (let i = 0; i < cardsArr.length; i++) {
    favorFunc(cardsArr[i]);
  }

  return (
    <div className="cards-container">
      <ProductsList addToBagBtn={true} arr={favouriteCardsArr} />
    </div>
  );
};

export default Favourites;
