import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import ProductsList from "../ProductsList/ProductsList";
import ControlledLogin from "../Form/Form";

const Bag = ({ clothId, setClothId, removeFromMyBag }) => {
  const cardsArr = useSelector((store) => store.app.cardsArr);
  const favorites = useSelector((store) => store.app.favorites);
  const addedToBag = useSelector((store) => store.app.addedToBag);

  const [poppedForm, setPoppedForm] = useState(false);

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
    localStorage.setItem("addedToBag", JSON.stringify(addedToBag));
  });

  const addedToTheBagArr = [];

  const addToTheBagFunc = (item) => {
    addedToBag.forEach((code) => {
      if (item.code === code) {
        return addedToTheBagArr.push(item);
      }
    });
  };

  for (let i = 0; i < cardsArr.length; i++) {
    addToTheBagFunc(cardsArr[i]);
  }

  return (
    <>
      <div className="cards-container">
        <ProductsList
          ableToBeRemoved={true}
          cardCross={true}
          arr={addedToTheBagArr}
        />
      </div>
      {addedToBag.length > 0 && (
        <div className="checkout-btn-container">
          <button className="checkout-btn" onClick={setPoppedForm}>
            Go to checkout
          </button>
        </div>
      )}
      {poppedForm && (
        <ControlledLogin
          customerClothes={addedToTheBagArr}
          closeForm={() => {
            setPoppedForm(false);
          }}
        />
      )}
    </>
  );
};

export default Bag;
