import React from "react";
import { Field, reduxForm } from "redux-form";
import renderField from './customField'
import { useDispatch, useSelector } from "react-redux";
import {
  SEND_CUSTOMER_DATA,
  CLEAR_CUSTOMER_BAG,
} from "../../redux/actions/types";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validEmail = (input) =>
  !EMAIL_REGEX.test(input) && "Please enter a valid email";

const requiredInput = (value) => (value ? undefined : "This field is required");

const validPhone = (value) =>
  value && isNaN(Number(value)) ? "Phone is incorrect" : undefined;

const validAge = (value) =>
  (value && isNaN(Number(value))) || value <= 0
    ? "Age is incorrect"
    : undefined;

let ControlledLogin = ({ closeForm, valid, customerClothes }) => {
  const dispatch = useDispatch();
  const customerData = useSelector((store) => store.app.customerData);

  const handleBlur = (e) => {
    console.log(e.target);
    const { name, value } = e.target;
    customerData[name] = value;
  };

  return (
    <div
      onClick={(e) =>
        e.target.classList.contains("dark-screen-container")
          ? closeForm()
          : null
      }
      className="dark-screen-container"
    >
      <form
        className="form"
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <h2 className="form__header">Purchase Form</h2>

        <Field
          name="name"
          placeholder="Name"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />

        <Field
          name="surname"
          placeholder="Surname"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="age"
          placeholder="Age"
          validate={[requiredInput, validAge]}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="adress"
          placeholder="Adress"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="phone"
          placeholder="Phone"
          validate={[requiredInput, validPhone]}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="email"
          placeholder="Email"
          validate={[requiredInput, validEmail]}
          component={renderField}
          onBlur={handleBlur}
        />
        <button
          onClick={() => {
            closeForm();
            localStorage.setItem("addedToBag", "[]");
            dispatch({ type: SEND_CUSTOMER_DATA, payload: customerData });
            dispatch({
              type: CLEAR_CUSTOMER_BAG,
              payload: [],
            });
            console.log(customerData);
            console.log(customerClothes);
          }}
          disabled={!valid}
          className="form__btn"
          type="submit"
        >
          Checkout
        </button>
      </form>
    </div>
  );
};

ControlledLogin = reduxForm({
  form: "Login",
})(ControlledLogin);

export default ControlledLogin;
