import React from "react";

const renderField = ({ input, placeholder, meta }) => (
  <div className="form__div_inp">
    <input
      {...input}
      placeholder={placeholder}
      className="form__inp"
      type="text"
    />
    {meta.touched && meta.error && (
      <span className="form__error">{meta.error}</span>
    )}
  </div>
);

export default renderField;
