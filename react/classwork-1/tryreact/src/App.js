import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.scss";
import LoginLogic from "./components/LoginLogic";

// import ButtonFunc from "./components/ButtonFunc";
// import Box from "./components/Box";

function App() {
  const [active, setActive] = useState(false);
  const [loggedIn, setLoggedIn] = useState(false);
  return (
    <div>
      <LoginLogic loggedIn={loggedIn} loggedInHandler={setLoggedIn} />
    </div>
  );

  // <div>
  //   <Box active={active} />
  //   <ButtonFunc
  //     text={"great button2"}
  //     boxHandler={setActive}
  //     active={active}
  //   />
  // </div>
}

export default App;
