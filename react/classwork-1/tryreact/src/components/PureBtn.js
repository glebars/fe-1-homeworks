import React from "react";

const PureBtn = ({ loggedIn, loggedInHandler }) => {
  const btnClick = () => {
    loggedInHandler(!loggedIn);
  };

  return <button onClick={btnClick}>{loggedIn ? "Log out" : "Log in"}</button>;
};

export default PureBtn;
