import React, { useState } from "react";

const ButtonFunc = ({ text = "btn", boxHandler, active }) => {
  const [hover, setHover] = useState("btn");
  
  const hoverStart = () => {
    setHover("btn btn_beautiful");
  };

  const hoverEnd = () => {
    setHover("btn");
  };
  return (
    <button
      className={hover}
      onClick={() => {
        boxHandler(!active);
      }}
      onMouseEnter={hoverStart}
      onMouseLeave={hoverEnd}
    >{`${active}`}</button>
  );
};
export default ButtonFunc;
