import React from "react";
import "./styles/App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends React.Component {
  state = {
    openedFirstModal: false,
    openedSecondModal: false,
  };
  render() {
    return (
      <div>
        <Button
          text="Open first modal"
          modalHandler={() => {
            this.setState({
              openedFirstModal: true,
            });
          }}
          bg="red"
        />
        <Button
          text="Open second modal"
          bg="aqua"
          modalHandler={() => {
            this.setState({
              openedSecondModal: true,
            });
          }}
        />
        {
          this.state.openedFirstModal &&
          <Modal
            header="First modal"
            closeButton={true}
            text="Once you delete this file, it won't be possible to undo this action.
              Are you sure you want to delet it?"
            actions={
              <>
                <button style={{backgroundColor: "darkred"}} className="modal__main-part-btn">Cancel</button>
                <button style={{backgroundColor: "darkred"}} className="modal__main-part-btn">Ok</button>
              </>
            }
            opened={this.state.openedFirstModal}
            mainBg='red'
            headerBg = 'darkred'
            closeModal={() =>{
              this.setState({
                openedFirstModal: false
              })
            }}
          />
        }
        {
          this.state.openedSecondModal &&
          <Modal
            header="Second modal?"
            closeButton={true}
            text="React is an open-source, front end, JavaScript library for building user interfaces or UI components."
            actions={
              <>
                <button style={{backgroundColor: "blue"}} className="modal__main-part-btn">Like</button>
                <button style={{backgroundColor: "blue"}} className="modal__main-part-btn">Share</button>
              </>
            }
            opened={this.state.openedSecondModal}
            mainBg='aqua'
            headerBg = 'blue'
            closeModal={() =>{
              this.setState({
                openedSecondModal: false
              })
            }}
          />
        }
      </div>
    );
  }
}

export default App;
