import React, { Component } from "react";

class Button extends Component {
  render() {
    const { text, modalHandler, bg } = this.props;
    return (
      <button
        onClick={modalHandler}
        className="btn"
        style={{ backgroundColor: bg }}
      >
        {text}
      </button>
    );
  }
}


export default Button;
