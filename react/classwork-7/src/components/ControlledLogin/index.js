import react, { useEffect, useState } from "react";
import { Field, reduxForm } from "redux-form";

let ControlledLogin = (props) => {
  const [values, setValues] = useState({
    login: "Admin",
    password: "",
    passwordConfirm: "",
  });

  // useEffect(() => {
  //   validateForm();
  // }, [values]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    setValues({ ...values, [name]: value });
  };

  const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  const requiredInput = (input) => !input && "Field required";
  const isEmail = (input) =>
    !EMAIL_REGEX.test(input) && "Please enter a valid email";
  const passwordMatch = (input, values) =>
    input !== values.password && "Passwords don't match";

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        console.log(props);
      }}
    >
      <Field
        type="text"
        placeholder="Login"
        name="login"
        component="input"
        validate={[requiredInput, isEmail]}
        onChange={handleChange}
        value={values.login}
      />
      {/* <p className="error">{errors.login}</p> */}

      <Field
        type="password"
        placeholder="Type pass"
        name="password"
        component="input"
        onChange={handleChange}
        // value={values.password}
      />
      {/* <p className="error">{errors.password}</p> */}

      <Field
        type="password"
        // onChange={handleChange}
        placeholder="Confirm pass"
        name="passwordConfirm"
        component="input"
        // value={values.passwordConfirm}
      />
      {/* <p className="error">{errors.confirmPassword}</p> */}

      <button type="submit">Submit form</button>
    </form>
  );
};

ControlledLogin = reduxForm({
  form: "Login",
})(ControlledLogin);

export default ControlledLogin;
