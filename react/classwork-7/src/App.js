import "./App.css";
import Login from "./components/Login";
import ControlledLogin from "./components/ControlledLogin";

const handlerSubmit = (e) => {
  e.preventDefault();
  // validateForm();
};

function App() {
  return (
    <div className="App">
      <ControlledLogin handlerSubmit={handlerSubmit}></ControlledLogin>
    </div>
  );
}

export default App;
