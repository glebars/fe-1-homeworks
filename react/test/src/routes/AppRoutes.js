import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import PropTypes from "prop-types";
import Home from "../components/Home/Home";
import Favourites from "../components/Favourites/Favourites";
import Bag from "../components/Bag/Bag";
import Modal from "../components/Modal/Modal";
import { getClothes } from "../redux/actions/data";
import { useSelector, useDispatch } from "react-redux";
import {
  REMOVE_FROM_BAG,
  ADD_TO_BAG,
  CLOSE_MODAL,
} from "../redux/actions/types";

const AppRoutes = () => {
  const { cardsArr, openedModal, addedToBag } = useSelector((store) => {
    return store;
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getClothes());
    localStorage.setItem("addedToBag", JSON.stringify(addedToBag));
  }, []);

  const [clothId, setClothId] = useState("");

  const addToMyBag = (cardId) => {
    if (addedToBag.includes(cardId)) {
      dispatch({
        type: REMOVE_FROM_BAG,
        payload: [...addedToBag],
      });
    } else {
      console.log(cardId);
      dispatch({
        type: ADD_TO_BAG,
        payload: [...addedToBag, cardId],
      });
    }
  };

  const modal = (
    <div>
      {openedModal && (
        <Modal
          header="Want to buy this item?"
          closeButton={true}
          text="This modal is made for confirming the addition of chosen item into your shopping bag. In order to add this item press 'Ok', otherwise 'Cancel'"
          actions={
            <>
              <button
                onClick={() => dispatch({ type: CLOSE_MODAL })}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Cancel
              </button>
              <button
                onClick={(e) => {
                  dispatch({ type: CLOSE_MODAL });
                  addToMyBag(clothId);
                }}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Ok
              </button>
            </>
          }
          mainBg="white"
          headerBg="white"
          closeModal={() => {
            dispatch({ type: CLOSE_MODAL });
          }}
        />
      )}
    </div>
  );

  return (
    <>
      <Route
        path="/favourites"
        exact
        render={() => <Favourites modal={modal} setClothId={setClothId} />}
      />
      <Route
        path="/bag"
        exact
        render={() => <Bag clothId={clothId} setClothId={setClothId} />}
      />
      <Route
        path="/"
        exact
        render={() => <Home modal={modal} setClothId={setClothId} />}
      />
    </>
  );
};

export default AppRoutes;
