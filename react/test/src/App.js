import React, { createContext } from "react";
import "./styles/App.scss";
import HeaderMenu from "./components/HeaderMenu/HeaderMenu";
import AppRoutes from "./routes/AppRoutes";
import { createStore } from "redux";

const data = {
  user: "Admin",
  format: "123",
};

export const ContextData = createContext(data.user);

function App() {
  return (
    <ContextData.Provider value={data.user}>
      <HeaderMenu />
      <AppRoutes />
    </ContextData.Provider>
  );
}

export default App;
