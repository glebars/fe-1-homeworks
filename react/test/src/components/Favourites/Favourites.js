import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Card from "../Card/Card";
import {REMOVE_FROM_FAVOURITES, OPEN_MODAL} from '../../redux/actions/types'
import { useSelector, useDispatch } from "react-redux";

const Favourites = ({ modal, setClothId }) => {
  const { cardsArr, favorites } = useSelector((store) => {
    return store;
  });

  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
  });

  const toggleFavorites = (cardId) => {
    if (favorites.includes(cardId)) {
      dispatch({
        type: REMOVE_FROM_FAVOURITES,
        payload: favorites.filter((id) => id !== cardId),
      });
    }
  };

  const favouriteCardsArr = [];

  const favorFunc = (item) => {
    favorites.forEach((code) => {
      if (item.code === code) {
        return favouriteCardsArr.push(item);
      }
    });
  };

  for (let i = 0; i < cardsArr.length; i++) {
    favorFunc(cardsArr[i]);
  }

  const listItems = favouriteCardsArr.map((cloth) => (
    <div className="card" key={cloth.code}>
      <Card
        toggleFavorites={toggleFavorites}
        cloth={cloth}
        filledStar={favorites.includes(cloth.code)}
        cardCross={false}
        openModal={() => {
          dispatch({ type: OPEN_MODAL });
        }}
      />
      <Button
        text="Add to cart"
        modalHandler={(clothId) => {
          dispatch({ type: OPEN_MODAL });
          setClothId(clothId);
        }}
        clothId={cloth.code}
        bg="black"
      />
    </div>
  ));

  return (
    <div className="cards-container">
      {listItems}
      {modal}
    </div>
  );
};

Favourites.propTypes = {
  modal: PropTypes.node.isRequired,
};

export default Favourites;
