import React from 'react'
import Button from './Button'
import { render, screen } from '@testing-library/react'

test('Check btn', () =>{
  const BTN_TEXT = 'Some test text';
  render(<Button text={BTN_TEXT} clothId='1000' modalHandler={() =>{}}></Button>)

  expect(screen.queryByText(BTN_TEXT)).toBeInTheDocument()
})