import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import { getClothes } from "./data";
import axips from "axios";

const openModal = () => ({ type: "OPEN_MODAL" });

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it("Check async action", () => {
  const defaultState = {};
  const store = mockStore(defaultState);

  store.dispatch(openModal());

  const actions = store.getActions();
  const expectedPayload = { type: "OPEN_MODAL" };

  expect(actions).toEqual([expectedPayload]);
});

it("Try fetch data from API", async () => {
  const defaultState = {};
  const store = mockStore(defaultState);

  console.log(await getClothes);

  const getClothes = () => {
    return (dispatch) => {
      axios
        .get("/clothesArr.json")
        .then((response) => {
          store.dispatch(getClothesSuccessCase(response.data));
        })
        .catch((err) => {
          store.dispatch(getFilmsFailureCase(err));
        });
    };
  };

  const getClothesSuccessCase = (cardsArr) => ({
    type: "REQUEST_CLOTHES_SUCCESS",
    payload: cardsArr,
  });

  const getFilmsFailureCase = (error) => ({
    type: "REQUEST_CLOTHES_FAILURE",
    payload: {
      ...error,
    },
  });
const actions = store.getActions()

  expect(actions).toEqual(getClothesSuccessCase())
});
