import logo from "./logo.svg";
import "./App.css";
import LoginPage from "./components/LoginPage";
import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div>
      <Switch>
        <Route path="/login">
          <LoginPage />
        </Route>

        <Route path="/">
          <div>
            <p>Home page</p>
          </div>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
