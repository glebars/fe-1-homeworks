import React from "react";
import "./styles/App.scss";
import HeaderMenu from "./components/HeaderMenu/HeaderMenu";
import GetData from "./components/GetData/GetData";

function App() {
  return (
    <>
      <HeaderMenu />
      <GetData />
    </>
  );
}

export default App;
