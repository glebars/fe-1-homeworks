import styles from "./App.module.css";
import CreateNotes from "./components/CreateNotes";
import ToDoList from "./components/ToDoList";

function App() {
  return (
    <div className={styles.container}>
      <h2>To do</h2>
      <CreateNotes></CreateNotes>
      <ToDoList test={true}></ToDoList>
    </div>
  );
}

export default App;
