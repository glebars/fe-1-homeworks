import reducer from "./index";

describe("Testing root reducer", () => {
  test("Test INCREMENT_CODE", () => {
    expect(reducer({ code: 0 }, { type: "INCREMENT_CODE" })).toStrictEqual({
      code: 1,
    });
  });
  test("Test create todo ", () => {
    expect(
      reducer({listOftodo: []}, { type: "CREATE_TODO", payload: "test string" })
    ).toStrictEqual({ listOftodo: ["test string"] });
  });
  test('test default case', ()=>{
    expect(
      reducer({}, {type: 'SOME_TEST_ACTION'})
    ).toStrictEqual({})
  })
});
