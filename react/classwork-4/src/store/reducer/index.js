import { INCREMENT_CODE } from "../constatnts";

const initialStore = {
  user: "Admin",
  code: 3310,
  listOftodo: [],
};

const reducer = (store = initialStore, action) => {
  switch (action.type) {
    case INCREMENT_CODE:
      return { ...store, code: store.code + 1 };
    case "CREATE_TODO":
      return { ...store, listOftodo: [...store.listOftodo, action.payload] };
    default:
      return store;
  }
};

export default reducer;
