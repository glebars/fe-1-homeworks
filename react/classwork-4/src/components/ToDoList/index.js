import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
// import style from "./style.module.css"
// import {styles} from './style'
import styled from "styled-components";

const mapStoreToProps = ({ listOftodo }) => {
  return {
    list: listOftodo,
  };
};

const ToDoListItem = styled.div`
  background: gray;
`;

const ToDoListStyles = styled.div`
  border: 1px solid black;
`;

class ToDoList extends Component {
  render() {
    const itemClass = classNames("todo-list__item", {
      "todo-list__item": this.props.test,
    });

    const { list } = this.props;
    return (
      <ToDoListStyles>
        <div className="todo-list">
          <ToDoListItem>
            {list.map(({ text }) => (
              <div className="1">
                <span>{text}</span>
              </div>
            ))}
          </ToDoListItem>
        </div>
      </ToDoListStyles>
    );
  }
}
ToDoList.defaultProps = {
  list: [{ text: "No todo" }],
};
export default connect(mapStoreToProps)(ToDoList);
