import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { INCREMENT_CODE } from "../../store/constatnts";

// const mapStoreToProps = ({ user, code, listOftodo }) => {
//   return {
//     currentUser: user,
//     code: code,
//     listOftodo: listOftodo,
//   };
// };
//
// const mapDispatchToProps = (dispatch) => {
//   return {
//     incrementCode: () => dispatch({ type: "INCREMENT_CODE" }),
//     createToDo: (toDoText) =>
//       dispatch({ type: "CREATE_TODO", payload: { text: toDoText } }),
//   };
// };

const CreateNotes = () => {
  const { user, code, incrementCode, createToDo } = useSelector((store) => {
    return store;
  });
  const [toDoText, setToDoText] = useState("first");
  const dispatch = useDispatch();

  return (
    <div className="">
      <input
        type="text"
        value={toDoText}
        onChange={(e) => {
          this.setState({
            toDoText: e.target.value,
          });
        }}
      />
      <button
        style={{
          border: "1px solid gray",
          borderRadius: "5px",
          cursor: "pointer",
          backgroundColor: "lightgray",
          padding: "8px 24px",
        }}
        onClick={() => {
          dispatch({ type: "CREATE_TODO", payload: { text: toDoText } });
        }}
      >
        Create
      </button>
      <p>{user}</p>
      <p>{code}</p>
      <button
        onClick={() => {
          dispatch({ type: "INCREMENT_CODE" });
        }}
      >
        Increment code
      </button>
    </div>
  );
};

export default CreateNotes;
