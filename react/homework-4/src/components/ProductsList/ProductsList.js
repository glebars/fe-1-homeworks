import React, {useState} from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  REMOVE_FROM_FAVOURITES,
  ADD_TO_FAVOURITES,
  OPEN_MODAL,
  CLOSE_MODAL,
  REMOVE_FROM_BAG,
  ADD_TO_BAG,
} from "../../redux/actions/types";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import Card from "../Card/Card";

const ProductsList = ({ arr, cardCross, addToBagBtn, ableToBeRemoved }) => {
  const favorites = useSelector((store) => store.favorites);
  const addedToBag = useSelector((store) => store.addedToBag);
  const openedModal = useSelector((store) => store.openedModal);
  const dispatch = useDispatch();

  const [clothId, setClothId] = useState('')

  const toggleFavorites = (cardId) => {
    if (favorites.includes(cardId)) {
      dispatch({
        type: REMOVE_FROM_FAVOURITES,
        payload: favorites.filter((id) => id !== cardId),
      });
    } else {
      dispatch({
        type: ADD_TO_FAVOURITES,
        payload: [...favorites, cardId],
      });
    }
  };

  const removeFromMyBag = (cardId) => {
    const cardIndex = addedToBag.indexOf(cardId);
    delete addedToBag[cardIndex];
  };

  const addToMyBag = (cardId) => {
    if (addedToBag.includes(cardId)) {
      dispatch({
        type: REMOVE_FROM_BAG,
        payload: [...addedToBag],
      });
    } else {
      console.log(cardId);
      dispatch({
        type: ADD_TO_BAG,
        payload: [...addedToBag, cardId],
      });
    }
  };

  const listItems = arr.map((cloth) => (
    <div className="card" key={cloth.code}>
      <Card
        setClothId={setClothId}
        toggleFavorites={toggleFavorites}
        cloth={cloth}
        filledStar={favorites.includes(cloth.code)}
        cardCross={cardCross}
        openModal={() => {
          dispatch({ type: OPEN_MODAL });
        }}
      />
      {addToBagBtn && (
        <Button
          text="Add to cart"
          modalHandler={(clothId) => {
            dispatch({ type: OPEN_MODAL });
            setClothId(clothId);
            console.log(clothId);
          }}
          clothId={cloth.code}
          bg="black"
        />
      )}
    </div>
  ));

  return (
    <>
      {listItems}
      {openedModal && (
        <Modal
          header="Want to buy this item?"
          closeButton={true}
          text="This modal is made for confirming the removing of chosen item from your shopping bag. In order to remove this item press 'Ok', otherwise 'Cancel'"
          actions={
            <div>
              <button
                onClick={() => dispatch({ type: CLOSE_MODAL })}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Cancel
              </button>
              <button
                onClick={(e) => {
                  dispatch({ type: CLOSE_MODAL });
                  ableToBeRemoved ? removeFromMyBag(clothId) : addToMyBag(clothId)
                }}
                style={{ backgroundColor: "white" }}
                className="modal__main-part-btn"
              >
                Ok
              </button>
            </div>
          }
          mainBg="white"
          headerBg="white"
          closeModal={() => {
            dispatch({ type: CLOSE_MODAL });
          }}
        />
      )}
    </>
  );
};

export default ProductsList;
