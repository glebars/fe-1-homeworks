import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import ProductsList from "../ProductsList/ProductsList";

const Bag = ({ clothId, setClothId, removeFromMyBag }) => {
  const cardsArr = useSelector((store) => store.cardsArr);
  const favorites = useSelector((store) => store.favorites);
  const addedToBag = useSelector((store) => store.addedToBag);

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
    localStorage.setItem("addedToBag", JSON.stringify(addedToBag));
  },);

  const addedToTheBagArr = [];

  const addToTheBagFunc = (item) => {
    addedToBag.forEach((code) => {
      if (item.code === code) {
        return addedToTheBagArr.push(item);
      }
    });
  };

  for (let i = 0; i < cardsArr.length; i++) {
    addToTheBagFunc(cardsArr[i]);
  }

  return (
    <div className="cards-container">
      <ProductsList ableToBeRemoved={true} cardCross={true} arr={addedToTheBagArr} />
    </div>
  );
};

export default Bag;
