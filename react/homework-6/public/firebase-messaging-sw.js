importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.4.1/firebase-messaging.js');
firebase.initializeApp({
  apiKey: "AIzaSyBEXoH7NPLvNAO2eYz8U2iobZ_h89REyPg",
  authDomain: "mypwa-64a84.firebaseapp.com",
  projectId: "mypwa-64a84",
  storageBucket: "mypwa-64a84.appspot.com",
  messagingSenderId: "243230825780",
  appId: "1:243230825780:web:af6ee212386473a1ac9961",
  measurementId: "G-KFDSGELKYF"
});


self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  // fcp_options.link field from the FCM backend service goes there, but as the host differ, it not handled by Firebase JS Client sdk, so custom handling
  if (event.notification && event.notification.data && event.notification.data.FCM_MSG && event.notification.data.FCM_MSG.notification) {
      const url = event.notification.data.FCM_MSG.notification.click_action;
      event.waitUntil(
          self.clients.matchAll({type: 'window'}).then( windowClients => {
              // Check if there is already a window/tab open with the target URL
              for (var i = 0; i < windowClients.length; i++) {
                  var client = windowClients[i];
                  // If so, just focus it.
                  if (client.url === url && 'focus' in client) {
                      return client.focus();
                  }
              }
              // If not, then open the target URL in a new window/tab.
              if (self.clients.openWindow) {
                  console.log("open window")
                  return self.clients.openWindow(url);
              }
          })
      )
  }
}, false);

const messaging = firebase.messaging();