import React from "react";
import "./styles/App.scss";
import HeaderMenu from "./components/HeaderMenu/HeaderMenu";
import AppRoutes from "./routes/AppRoutes";

function App() {
  return (
    <>
      <HeaderMenu />
      <button className='push-notifications_btn' onClick={() => {
        fetch("https://fcm.googleapis.com/fcm/send",{
          method: 'POST',
              headers: {
                "Authorization": "key=AAAAOKGv3TQ:APA91bEgXRNVN64xOoJSO4bNLq-xyr2EAWEZVu8J3_LkFh4y7uY6yi3KeW528gDI7BWGQrIOhD5Imlbx3-5HWPqVLB0yc32vwSJxZQQGLEQPJMFcOnVkko-IDOt-qxSNjhB6v6JL2sJ5",
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "notification": {
              "title": "Test",
              "body": "This is push notification",
              "click_action": "https://b0arding.com/",
              "icon": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQkePDu56HZ1aRg0J0_WZT_L8n5VNMXqpYrRg&usqp=CAU"
            },
            "to": "f9qx7bvgHL8eIDgICs1gkf:APA91bF8PhZQmY-kYLyYE4QRrwDZ8Wxw2VpBnMmcIEy28zZHIvMcf3rdCfAD9kZ4RaBGGw0lqRYDwNHJrWWmYakXYQkDulgjh8aBwMw35-ueyrA2WpRAnNKWQnoTPAUCVkOq5X2DLRoM"
          })
      }).then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
      }}>Click me to get push notification</button>
      <AppRoutes />
      </>
  );
}

export default App;
