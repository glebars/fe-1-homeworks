import React from "react";
import Star from "./Star";
import { fireEvent, getByRole, render, screen } from "@testing-library/react";

test("check star presence", () => {
  render(<Star cloth={{}} toggleFavorites={() => {}} filledStar={true} />);
  expect(screen.getByRole("star-svg")).toBeInTheDocument();
});

test("Check star function calling", () => {
  const toggleFavorites = jest.fn();
  render(
    <Star cloth={{}} toggleFavorites={toggleFavorites} filledStar={true} />
  );
  fireEvent.click(screen.getByRole("star-container"));
  expect(toggleFavorites).toHaveBeenCalledTimes(1);
});

test("Check star filling", () => {
  render(
    <Star
      role="star-svg"
      cloth={{}}
      toggleFavorites={() => {}}
      filledStar={true}
    />
  );

  expect(screen.getByRole(/star-svg/i)).toHaveAttribute('fill', 'black');
});
