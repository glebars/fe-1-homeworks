import React, { useEffect } from "react";
import ProductsList from "../ProductsList/ProductsList";
import { useSelector } from "react-redux";
import useIsIOS from "../customHooks/useIsIOS";
import InstallPWA from '../InstallationModal/InstallationModal'

const Home = ({ addToMyBag, clothId, setClothId }) => {
  const { prompt } = useIsIOS();

  const cardsArr = useSelector((store) => store.app.cardsArr);
  const favorites = useSelector((store) => store.app.favorites);
  const addedToBag = useSelector((store) => store.app.addedToBag);

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorites));
    localStorage.setItem("addedToBag", JSON.stringify(addedToBag));
  });

  return (
    <div className="cards-container">
      <ProductsList addToBagBtn={true} arr={cardsArr} />
      {prompt && <InstallPWA/>}
    </div>
  );
};

export default Home;
