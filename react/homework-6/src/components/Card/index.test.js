import React from "react";
import Card from "./Card";
import { fireEvent, render, screen } from "@testing-library/react";

test("Check card cross presence", () => {
  const { getByRole } = render(
    <Card
      cloth={{}}
      toggleFavorites={() => {}}
      filledStar={false}
      cardCross={true}
      openModal={() => {}}
      setClothId={() => {}}
    />
  );
  expect(screen.getByRole("remove")).toBeInTheDocument();
});

test("Check card cross func", () => {
  const { getByRole } = render(
    <Card
      cloth={{}}
      toggleFavorites={() => {}}
      filledStar={false}
      cardCross={true}
      openModal={() => {}}
      setClothId={() => {}}
    />
  );
  expect(screen.getByRole("remove")).not.toHaveTextContent()
  expect(screen.getByRole("remove")).toHaveClass('modal__cross_card')
});
