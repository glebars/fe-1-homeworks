import React from "react";
import Button from "./Button";
import { fireEvent, render, screen } from "@testing-library/react";

test("Check button presence", () => {
  render(<Button text='Add to card' clothId='1000' modalHandler={() =>{}}/>);

  expect(screen.queryByText("Add to card")).toBeInTheDocument();
});

test("Check button clicking", () => {
  const modalHandler = jest.fn()
  render(<Button text='Add to card' clothId='1000' modalHandler={modalHandler}/>);
  fireEvent.click(screen.queryByText("Add to card"))
  expect(modalHandler).toHaveBeenCalledTimes(1)
});
