import React from "react";
import PropTypes from "prop-types";

const Button = ({
  text = "Add to card",
  modalHandler = () => {},
  bg = "gray",
  clothId = "1000",
}) => {
  return (
    <button
      onClick={() => {
        modalHandler(clothId);
      }}
      className="btn"
      style={{ backgroundColor: bg }}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  modalHandler: PropTypes.func.isRequired,
  bg: PropTypes.string,
  clothId: PropTypes.string.isRequired,
};

export default Button;
