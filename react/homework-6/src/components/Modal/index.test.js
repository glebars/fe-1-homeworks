import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";
import Modal from "./Modal";
import { useDispatch } from "react-redux";
import { CLOSE_MODAL } from "../../redux/actions/types";

test("Check modal presence", () => {
  const { getByText } = render(
    <Modal
      header=""
      closeButton={true}
      text=""
      actions={<div></div>}
      headerBg="white"
      mainBg="white"
      closeModal={() => {}}
    />
  );
  expect(screen.getByRole("modal")).toHaveClass("dark-screen-container");
  expect(screen.getByRole("modal")).toBeInTheDocument();
  expect(screen.getByRole("modal")).not.toBeEmptyDOMElement();
});

test("Check modal title ", () => {
  const { getByText } = render(
    <Modal
      header="Want to buy this item?"
      closeButton={true}
      text=""
      actions={<div></div>}
      headerBg="white"
      mainBg="white"
      closeModal={() => {}}
    />
  );
  expect(getByText("Want to buy this item?")).toBeInTheDocument();
  expect(getByText("Want to buy this item?")).toContainHTML(
    '<h4 class="modal__header-section-text">Want to buy this item?</h4>'
  );
});

// const closeModal =()=>{
//   const dispatch = useDispatch()
//   dispatch({ type: CLOSE_MODAL })
// }

// test("Check modal closing ", () => {
//   render(
//     <Modal
//       header=""
//       closeButton={true}
//       text=""
//       actions={<div></div>}
//       headerBg="white"
//       mainBg="white"
//       closeModal={closeModal}
//     />
//   );
//   fireEvent.click(screen.queryByRole('modal-cross'))
//   expect(screen.getByRole("modal")).not.toBeInTheDocument()
// });

