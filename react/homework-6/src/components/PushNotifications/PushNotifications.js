import firebase from 'firebase';

export const initializeFirebase = () => {
  firebase.initializeApp({
    apiKey: "AIzaSyBEXoH7NPLvNAO2eYz8U2iobZ_h89REyPg",
    authDomain: "mypwa-64a84.firebaseapp.com",
    projectId: "mypwa-64a84",
    storageBucket: "mypwa-64a84.appspot.com",
    messagingSenderId: "243230825780",
    appId: "1:243230825780:web:af6ee212386473a1ac9961",
    measurementId: "G-KFDSGELKYF"
  });
}

export const askForPermissionToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    console.log('Your token is:', token);
    
    return token;
  } catch (error) {
    console.error(error);
  }
}

window.addEventListener('DOMContentLoaded', askForPermissionToReceiveNotifications)