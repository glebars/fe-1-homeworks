import React, { useState } from "react";
import { Field, reduxForm } from "redux-form";
import {  useDispatch } from "react-redux";
import { SEND_CUSTOMER_DATA, CLEAR_CUSTOMER_BAG } from "../../redux/actions/types";

/// my custom input
const renderField = ({ input, placeholder, meta }) => (
  <div className="form__div_inp">
    <input
      {...input}
      placeholder={placeholder}
      className="form__inp"
      type="text"
    />
    {meta.touched && meta.error && (
      <span className="form__error">{meta.error}</span>
    )}
  </div>
);
///

/// validation funcs
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const validEmail = (input) =>
  !EMAIL_REGEX.test(input) && "Please enter a valid email";

const requiredInput = (value) => (value ? undefined : "This field is required");

const validPhone = (value) =>
  value && isNaN(Number(value)) ? "Phone is incorrect" : undefined;

const validAge = (value) =>
  (value && isNaN(Number(value))) || value <= 0
    ? "Age is incorrect"
    : undefined;

///

let ControlledLogin = ({ closeForm, valid, customerClothes }) => {

  const dispatch = useDispatch();

  const [values, setValues] = useState({
    name: "",
    surname: "",
    age: "",
    adress: "",
    phone: "",
    email: "",
  });

  const handleBlur = (e) => {
    console.log(e.target);
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  return (
    <div
      onClick={(e) =>
        e.target.classList.contains("dark-screen-container")
          ? closeForm()
          : null
      }
      className="dark-screen-container"
    >
      <form
        className="form"
      >
        <h2 className="form__header">Purchase Form</h2>
        <div onClick={closeForm} className='modal__cross'></div> 

        <Field
          name="name"
          placeholder="Name"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />

        <Field
          name="surname"
          placeholder="Surname"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="age"
          placeholder="Age"
          validate={[requiredInput, validAge]}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="adress"
          placeholder="Adress"
          validate={requiredInput}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="phone"
          placeholder="Phone"
          validate={[requiredInput, validPhone]}
          component={renderField}
          onBlur={handleBlur}
        />
        <Field
          name="email"
          placeholder="Email"
          validate={[requiredInput, validEmail]}
          component={renderField}
          onBlur={handleBlur}
        />
        <button
          onClick={() => {
            closeForm();
            localStorage.setItem('addedToBag', '[]')
            dispatch({ type: SEND_CUSTOMER_DATA, payload: values });
            dispatch({ type: CLEAR_CUSTOMER_BAG, payload:  localStorage.getItem('addedToBag')});
            console.log(values)
            console.log(customerClothes)

          }}
          disabled={!valid}
          className="form__btn"
          type="submit"
        >
          Checkout
        </button>
      </form>
    </div>
  );
};

ControlledLogin = reduxForm({
  form: "Login",
})(ControlledLogin);

export default ControlledLogin;
