import React, { useEffect } from "react";
import { Route } from "react-router-dom";
import Home from "../components/Home/Home";
import Favourites from "../components/Favourites/Favourites";
import Bag from "../components/Bag/Bag";
import { getClothes } from "../redux/actions/data";
import { useDispatch } from "react-redux";

const AppRoutes = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getClothes());
  }, []);

  return (
    <>
      <Route path="/favourites" exact component={Favourites} />
      <Route path="/bag" exact component={Bag} />
      <Route path="/" exact component={Home} />
    </>
  );
};

export default AppRoutes;
