import React, { Component } from "react";
import PropTypes from "prop-types";
import Star from "./Star";


class Card extends Component {
  state = {
    filledStar: false,
  };

  render() {
    const { cloth, toggleFavorites, filledStar } = this.props;

    return (
      <>
        <Star
          cloth={cloth}
          toggleFavorites={toggleFavorites}
          filledStar={filledStar}
        />
        <img className="card__img" src={cloth.url} alt="12321" />
        <h2 className="card__name">{cloth.name}</h2>
        <span className="card__color">{cloth.color}</span>
        <span className="card__price">{cloth.price}</span>
      </>
    );
  }
}

Card.propTypes = {
  cloth: PropTypes.object.isRequired,
  toggleFavorites: PropTypes.func.isRequired,
  filledStar: PropTypes.bool.isRequired,
};

export default Card;
