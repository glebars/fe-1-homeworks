import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "./Button";
import Modal from "./Modal";
import Card from "./Card";

class CardsList extends Component {
  state = {
    openedFirstModal: false,
    clothesList: [],
    clothId: "",
    favorites: JSON.parse(localStorage.getItem("favorites")) || [],
    addedToTheBag: JSON.parse(localStorage.getItem("addedToTheBag")) || [],
  };

  componentDidMount() {
    fetch("/clothesArr.json")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          clothesList: data,
        });
      });
  }

  render() {
    const { clothesList } = this.state;

    const listItems = clothesList.map((cloth) => (
      <div className="card" key={cloth.code}>
        <Card
          toggleFavorites={this.toggleFavorites}
          cloth={cloth}
          filledStar={this.state.favorites.includes(cloth.code)}
        />
        <Button
          text="Add to cart"
          modalHandler={(clothId) => {
            this.setState({
              openedFirstModal: true,
              clothId: clothId,
            });
            // console.log(clothId);
          }}
          clothId={cloth.code}
          bg="black"
        />
      </div>
    ));

    return (
      <div className="cards-container">
        {listItems}
        {this.state.openedFirstModal && (
          <Modal
            header="Want to buy this item?"
            closeButton={true}
            text="Once you delete this file, it won't be possible to undo this action.
              Are you sure you want to delete it?"
            actions={
              <>
                <button
                  onClick={() =>
                    this.setState({
                      openedFirstModal: false,
                    })
                  }
                  style={{ backgroundColor: "white" }}
                  className="modal__main-part-btn"
                >
                  Cancel
                </button>
                <button
                  onClick={(e) => {
                    this.setState({
                      openedFirstModal: false,
                    });
                    this.addToTheBag(this.state.clothId);
                  }}
                  style={{ backgroundColor: "white" }}
                  className="modal__main-part-btn"
                >
                  Ok
                </button>
              </>
            }
            opened={this.state.openedFirstModal}
            mainBg="white"
            headerBg="white"
            closeModal={() => {
              this.setState({
                openedFirstModal: false,
              });
            }}
          />
        )}
      </div>
    );
  }

  toggleFavorites = (cardId, star) => {
    console.log(cardId);
    const { favorites } = this.state;


    if (favorites.includes(cardId)) {
      this.setState(
        { favorites: favorites.filter((el) => el !== cardId) },
        () => {
          localStorage.setItem(
            "favorites",
            JSON.stringify(this.state.favorites)
          );
        }
      );
    } else {
      this.setState({ favorites: [...favorites, cardId] }, () => {
        localStorage.setItem("favorites", JSON.stringify(this.state.favorites));
      });
    }
  };

  addToTheBag = (cardId) => {
    // localStorage.removeItem("addedToTheBag");
    console.log(cardId)

    const { addedToTheBag } = this.state;

    if (addedToTheBag.includes(cardId)) {
      this.setState(
        { addedToTheBag: addedToTheBag.filter((el) => el !== cardId) },
        () => {
          localStorage.setItem(
            "addedToTheBag",
            JSON.stringify(this.state.addedToTheBag)
          );
        }
      );
    } else {
      this.setState({ addedToTheBag: [...addedToTheBag, cardId] }, () => {
        localStorage.setItem(
          "addedToTheBag",
          JSON.stringify(this.state.addedToTheBag)
        );
      });
    }
  };
}

CardsList.protoTypes = {
  clothesList: PropTypes.array.isRequired,
};

export default CardsList;
