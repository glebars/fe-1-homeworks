import React, { Component } from "react";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    const { text, modalHandler, bg, clothId } = this.props;
    return (
      <button
        onClick={() => {
          modalHandler(clothId);
          // console.log(clothId);
        }}
        className="btn"
        style={{ backgroundColor: bg }}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  modalHandler: PropTypes.func.isRequired,
  bg: PropTypes.string,
};

Button.defaultProps = {
  bg: "gray",
};

export default Button;
