import React from "react";
import "./styles/App.scss";
import CardsList from "./components/CardsList";

class App extends React.Component {
  render() {
    return (
      <div>
        <CardsList />
      </div>
    );
  }
}

export default App;
