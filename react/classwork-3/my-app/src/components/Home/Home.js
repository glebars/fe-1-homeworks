import React from 'react';
import SomeView from '../SomeView/SomeView'
import RefCheck from '../RefCheck/RefChech'
import * as Icons from '../../icons/index'

const Home = () => {
  return(
    <section>
      <h1>Home</h1>
      <SomeView/>
      <RefCheck/>
      {Icons.Smile({color: 'red', width: '55px'})}
    </section>
  );
};

export default Home;