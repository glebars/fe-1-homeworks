import React, {useRef} from 'react';

const RefCheck = () =>{
  const inputEl = useRef(null)

  const onButtonClick = () =>{
    inputEl.current.focus()
    console.log(inputEl)
  }

  return(
    <>
      <input ref={inputEl} type="text"/>
      <button onClick={onButtonClick}>Установить фокус</button>
    </>
  )
}

export default RefCheck