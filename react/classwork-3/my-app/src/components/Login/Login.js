import React, { useState } from "react";
import { Link } from "react-router-dom";
import styles from "./styles";

const Login = ({ setIsAuth, isAuth }) => {

  const [counter, setCounter] = useState(0);
  const [isActive, setIsActive] = useState(false);

  return (
    <section>
      <form style={styles}>
        <input type="text" />
        <input type="text" />
        <button
          onClick={(e) => {
            e.preventDefault();
            setIsAuth(!isAuth);
          }}
        >
          Login
        </button>
        <span>{counter}</span>
        <button onClick={(e) => {
          e.preventDefault()
          setCounter(counter + 1);
        }}>+1</button>
        <Link to="/home">Home</Link>
      </form>
    </section>
  );
};

export default Login;
