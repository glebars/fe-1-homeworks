import React from "react";
import { Route, Redirect, Router } from "react-router-dom";

const PrivateRoute = ({ isAuth, ...props }) => {
  return isAuth ? <Route {...props}></Route> : <Redirect to="/"> </Redirect>;
};

export default PrivateRoute;
