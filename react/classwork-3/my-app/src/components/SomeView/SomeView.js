import React, {useEffect, useState} from 'react';

const SomeView = () =>{
  const [count, setCount] = useState(0)

  useEffect(() =>{
    document.title = `You pressed ${count} times`
  })


  return(
    <div>
      <p>Test useEffect</p>
      <button onClick={() =>{
        setCount(count + 1)
      }}>
        Click for action
      </button>
    </div>

  )
}

export default SomeView
