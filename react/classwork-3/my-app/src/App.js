import "./App.css";
import { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";

function App() {
  const [isAuth, setIsAuth] = useState(false);
  return (
    <Router>
      <Switch>
        <PrivateRoute path="/home" isAuth={true}>
          <Home />
        </PrivateRoute>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/">
          <Login setIsAuth={setIsAuth} isAuth={isAuth} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
