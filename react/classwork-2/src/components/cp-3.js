import React, { Component } from "react";
import Prototypes from 'prop-types'


class CpThree extends Component {
  render() {
    const { data, dataHandler } = this.props;

    return (
      <div className="cp cp_three">
        <p>{data}</p>
        <span>cp three</span>
        <button
          className="btn"
          onClick={() => {
            dataHandler(data + 3);
          }}
        >
          Trigger
        </button>
      </div>
    );
  }
}

CpThree.defaultProps = {
  data: 100
}

CpThree.propTypes = {
  data: Prototypes.number,
  dataHandler: Prototypes.func,
  dataTest: Prototypes.exact({
    name: Prototypes.string,
    age: Prototypes.number,
    status: Prototypes.string,
    test: Prototypes.string
  })
}

export default CpThree;

// const cpThree = () =>{
//   return (
//     <div className='cp cp_three'>
//       <span>cp three</span>
//       <button>Trigger</button>
//     </div>
//   )
// }
