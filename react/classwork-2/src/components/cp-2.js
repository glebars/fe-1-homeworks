import React, { Component } from 'react' 
import CpThree from './cp-3'

class CpTwo extends Component {
  render() {
    const { data , dataHandler} = this.props
    return (
      <div className="cp cp_two">
        <span>cp two</span>
        <CpThree data ={data} dataHandler={dataHandler} dataTest={{

          name: 'Vlad',
          age: 101,
          status: 'admin',
          test: 'smth'
        }
        }/>
      </div>
    );
  }
}
export default CpTwo;

// const cpTwo = () =>{
//   return (
//     <div className='cp cp_two'>
//       <span>cp two</span>
//     </div>
//   )
// }
