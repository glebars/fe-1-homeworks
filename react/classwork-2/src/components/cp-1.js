import React, { Component } from "react";
import CpTwo from './cp-2'

class CpOne extends Component {
  render() {
    const { data, dataHandler, children } = this.props
    return (
      <div className="cp cp_one">
        <span>cp one</span>
        {children}
        <CpTwo data={data} dataHandler={dataHandler}/>
      </div>
    );
  }
}
export default CpOne;

// const cpOne = ({ data }) =>{
//   return (
//     <div className='cp cp_one'>
//       <span>cp one</span>
//     </div>
//   )
// }

