import React, { useState } from "react";
import "./App.css";
import CpOne from "./components/cp-1";

class App extends React.Component {
  state = {
    counter: 0,
  };

  counterHandler = (data) => {
    this.setState((state) => ({ counter: data }));
  };

  render() {
    return (
      <div>
        <span className="counter">{this.state.counter}</span>
        <CpOne data={this.state.counter} dataHandler={this.counterHandler}>
          <span>Some child</span>
        </CpOne>
        <button onClick={this.counterHandler}>Test</button>
      </div>
    );
  }
}

// function App() {
//   return (
//     <div>
//       <cpOne />
//     </div>
//   );
// }

export default App;
