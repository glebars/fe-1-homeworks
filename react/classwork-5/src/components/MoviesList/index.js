import React, { Component } from "react";
import { connect } from "react-redux";
import MovieCard from "../MovieCard/";
import { getFilms} from '../../store/actions/'

const mapStoreToProps = ({ movies }) => {
  return {
     movies: movies,
  };
};

class MoviesList extends Component {

  componentDidMount(){
    const dispatch  = this.props.dispatch
    dispatch(getFilms())
  }

  render() {
    const { movies = [{title: 'No cards'}] } = this.props;

    return (
      <div className="movies">
        {movies.map((item) => (
          <MovieCard key={item.edited} film={item.title} />
        ))}
      </div>
    );
  }
}

export default connect(mapStoreToProps)(MoviesList);
