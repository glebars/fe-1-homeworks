import React from 'react'

const MovieCard = ({film}) =>{
  return (
    <div className='movie-card'>
        <h1>{film}</h1>
    </div>
  )
}

export default MovieCard