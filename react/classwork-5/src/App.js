import logo from "./logo.svg";
import "./App.css";
import MoviesList from "./components/MoviesList";
import Loader from "./components/Loader/index";

function App() {
  return(
  <>
    <Loader></Loader>
    <MoviesList />
  </>
  )
}

export default App;
