import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import {
  REQUEST_MOVIES_FAILURE,
  REQUEST_MOVIE_SUCCESS,
  REQUEST_MOVIES_STARTED,
} from "./store/actions/types";

const initialStore = {
  loading: false,
  movies: []
};

const reducer = (store = initialStore, action) => {
  switch (action.type) {
    case REQUEST_MOVIES_STARTED:
      return {
        ...store,
        loading: true,
      };
    case REQUEST_MOVIE_SUCCESS:
      return {
        ...store,
        loading: false,
        error: null,
        movies: [...store.movies, ...action.payload],
      };
    case REQUEST_MOVIES_FAILURE:
      return {
        ...store,
        loading: false,
        error: action.payload,
      };
    default:
      return store;
  }
  return store;
};

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk)
);

const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
