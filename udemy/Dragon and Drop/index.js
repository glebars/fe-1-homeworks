// let container = document.querySelector('.container')
// let div
// let btn = document.querySelector('.btn')
//
// btn.addEventListener('click', () => {
//     div = document.createElement("div")
//     container.prepend(div)
//     div.classList.add('block')
//     div.addEventListener('mousedown', dragon)
//
// })
//
// function dragon (e) {
//     console.log(e);
//     let coords = getCoords(div);
//     let shiftX = e.pageX - coords.left;
//     let shiftY = e.pageY - coords.top;
//
//     div.style.position = 'absolute';
//     document.body.appendChild(div);
//     moveAt(e);
//
//     div.style.zIndex = 1000;
//
//     function moveAt(e) {
//         div.style.left = e.pageX - shiftX + 'px';
//         div.style.top = e.pageY - shiftY + 'px';
//     }
//
//     document.onmousemove = function (e) {
//         moveAt(e);
//     }
//
//     div.onmouseup = function () {
//         document.onmousemove = null;
//         div.onmouseup = null;
//     }
//     div.ondragstart = function () {
//         return false;
//     };
// }
//
//
// function getCoords(elem) {
//     let box = elem.getBoundingClientRect();
//     return {
//         top: box.top + pageYOffset,
//         left: box.left + pageXOffset
//     };
// }

"use strict"

import Button from "../classes/button";
import Select from "../classes/select";
import Input from "../classes/input";
import Form from "../classes/form";
import Modal from "../classes/modal";
import axios from "axios";

async function createCard(token, form) {
    let elems = form.elements;
    let obj = {};

    [...elems].forEach(el => {
        obj[el.name] = el.value;
    });

    return await axios.post('http://cards.danit.com.ua/cards', {
        obj
    }, {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}

async function getCards(token) {
    return await axios.get('http://cards.danit.com.ua/cards', {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
}

let content = document.querySelector('.content');

function renderCard(data) {
    let result = '';

    for (let dataKey in data.obj) {
        result += `<li>${data.obj[dataKey]}</li>`;
    }

    let wrapCard = document.createElement("div")
    wrapCard.classList.add("card")
    wrapCard.insertAdjacentHTML('afterbegin', `<ul>${result}</ul>`)
    content.append(wrapCard)
}



let register = document.querySelector('.register');
let loginBtn = new Button('Вход', 'btn', 'login-btn');

register.append(loginBtn.render());

let registerButton = document.getElementById('login-btn');

registerButton.addEventListener('click', () => {
    let signupLogin = new Input('text', 'input', 'login', 'E-mail');
    let signupPassword = new Input('password', 'input', 'password', 'Password');
    let signupBtn = new Button('Войти', 'btn', 'signup-btn');

    let modal = new Modal('Регистрация', [
        signupLogin.render(),
        signupPassword.render(),
        signupBtn.render()
    ]);

    content.insertAdjacentHTML('afterbegin', modal.render());

    let closePopupBtn = document.querySelector('.close');

    closePopupBtn.addEventListener('click', () => {
        modal.close();
    });

    let sendSignup = document.getElementById("signup-btn");
    sendSignup.addEventListener('click', () => {
        let email = document.getElementById('login');
        let password = document.getElementById('password');

        axios.post('http://cards.danit.com.ua/login', {
            email: email.value,
            password: password.value
        })
            //d36db9b6563b
            .then(function (response) {
                if (response.data.status === "Success") {
                    modal.close();

                    let allCards = getCards(response.data.token);
                    allCards
                        .then(cards => {

                            cards.data.forEach(el => {
                                renderCard(el)
                            })
                        })

                    register.innerHTML = '';

                    let createVisitBtn = new Button('Создать визит', 'btn', 'create-visit-btn');
                    createVisitBtn.render();
                    register.append(createVisitBtn.render());

                    let createVisit = document.getElementById('create-visit-btn');
                    createVisit.addEventListener('click', () => {

                        let selectDoctor = new Select([
                            'Кардиолог',
                            'Стоматолог',
                            'Терапевт'
                        ], 'select', 'select-doctor', 0, 'Выберите доктора');

                        let modal = new Modal('Создать визит', [
                            selectDoctor.render(),
                        ]);

                        content.insertAdjacentHTML('afterbegin', modal.render());

                        let doctor = selectDoctor.change();

                        doctor.addEventListener('change', () => {
                            let createVisitContent = document.querySelector('.popup__text');

                            while (createVisitContent.children.length > 1) {
                                createVisitContent.removeChild(createVisitContent.lastChild);
                            }

                            switch (doctor.value) {

                                case "кардиолог":
                                    let form = new Form('createVisitForm', [
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('text', 'input', 'press', 'Обычное давление').render(),
                                        new Input('text', 'input', 'mass', 'Индекс массы тела').render(),
                                        new Input('text', 'input', 'diseases', 'Перенесенные заболевания сердечно-сосудистой системы').render(),
                                        new Input('text', 'input', 'age', 'Возраст').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render()
                                    ]);

                                    createVisitContent.append(form.render());

                                    let createVisitBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitBtn.render());

                                    break;

                                case "стоматолог":

                                    let formDant = new Form('createVisitForm', [
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('text', 'input', 'last', 'Дата последнего посещения').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render()
                                    ]);

                                    createVisitContent.append(formDant.render());

                                    let createVisitDantBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitDantBtn.render());

                                    break;

                                case "терапевт":

                                    let formTerapeut = new Form('createVisitForm', [
                                        new Input('text', 'input', 'target', 'Цель визита').render(),
                                        new Input('text', 'input', 'desc', 'Краткое описание визита').render(),
                                        new Select([
                                            'Обычная',
                                            'Приоритетная',
                                            'Неотложная'
                                        ], 'select', 'urgency', 0, 'Выберите срочность').render(),
                                        new Input('text', 'input', 'age', 'Возраст').render(),
                                        new Input('text', 'input', 'fio', 'Ф.И.О.').render()
                                    ]);

                                    createVisitContent.append(formTerapeut.render());

                                    let createVisitTerapeutBtn = new Button('Создать визит', 'btn', 'create-visit-button');
                                    createVisitContent.append(createVisitTerapeutBtn.render());

                                    break;
                            }

                            let createVisitButton = document.getElementById("create-visit-button");

                            createVisitButton.addEventListener('click', () => {

                                let form = document.forms.createVisitForm;
                                let createResponse = createCard(response.data.token, form);

                                createResponse
                                    .then(data => {
                                        modal.close();

                                        renderCard(data.data)
                                    });
                            })
                        })
                    });
                } else {
                    console.log(response.data)
                }
            });
    })
});
