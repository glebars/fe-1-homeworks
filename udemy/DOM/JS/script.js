let boxes = document.getElementsByClassName("square")
let circles = document.getElementsByTagName("button")
// У querySelectorAll() есть цикл forEach(callback function)
// boxes[0].style.backgroundColor = 'blue';
// boxes[0].style.width = '200px';
// или
boxes[0].style.cssText = 'background-color: blue; height: 100px;' // задаём сразу несколько css свойств
circles[0].style.backgroundColor = 'orange'

let div = document.createElement('div') // создаём элемент в js
// let text = document.createTextNode('Любой текст')

div.classList.add('black') // даём название класса, созданному элементу
div.style.cssText = 'background-color: black; height: 150px; width: 100px; margin: 10px; color: white;'
document.body.append(div) //в конце body
// document.body.prepend(div) в начале body
// circles[0].after(div) ////ставим div после первым кругом
// circles[0].before(div) //ставим div перед первым кругом

// circles[1].remove() // удаляем второй круг
// div.replaceWith(boxes[0]); // заменяет div первой коробкой

// div.innerText = "Hello World!"
// div.innerHTML = "Hello World" // можно написать через HTML
div.innerHTML = "<h1>Hello World<h1>"//можно сделать, как загаловок
div.insertAdjacentHTML("afterbegin", '<p>My name is<p>') // где (div) куда(afterbegin) что(<p>) всавить
