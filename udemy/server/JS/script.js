//JSON
// const person = {
//     name: 'Alex',
//     tel: "+7615212541",
//     parents: {
//         mum: 'Olga',
//         dad: 'Oleg'
//     }
// }
//
// let clone = (JSON.parse(JSON.stringify(person)))
//
// clone.parents.mum = 'Irina'
//
// console.log(clone);
//
// const person1 = JSON.stringify(person)
//
// console.log(JSON.parse(person1))
//JSON

const inpUAH = document.querySelector('#uah')
const inpUSD = document.querySelector('#usd')

inpUAH.addEventListener('input', (e) =>{
    const request = new XMLHttpRequest();

    request.open('GET', 'js/current.json')
    request.setRequestHeader('Content-type', 'application/json')
    request.send()

    request.addEventListener("load", () =>{
        if (request.status === 200){
            console.log(request.response);
            const data = JSON.parse(request.response);
            inpUSD.value = (+inpUAH.value / data.current.usd).toFixed(2);
        } else{
            inpUSD.value = 'error'
        }
    })
    // inpUAH.addEventListener('focus', () =>{
    //     inpUSD.style.color = 'gray'
    // })
    inpUSD.style.color = 'gray'

    inpUAH.addEventListener('blur', () =>{
        inpUSD.style.color = 'black'
    })
})