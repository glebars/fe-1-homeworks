

// let a = 5;
//
// function first() {
//     let b = 10
//
//     function second() {
//         let c = 15;
//
//         function third() {
//             return a + b + c;
//         }
//
//         c = 0
//
//         return third()
//     }
//
//     return second()
// }
//
// let result = first()
//
// console.log(result);

// let result = []
//
// for (let i = 0; i < 5; i++){
//     result[i] = function () {
//         console.log(i);
//     }
// }
//
// result[0]()
// result[1]()
// result[2]()
// result[3]()
// result[4]()

// function globalTest(num, obj) {
//     let array = [1, 2, 3]
//
//     function doSome(i) {
//         num += i;
//
//         array.push(num)
//
//         console.log(`num: ${num}`)
//         console.log(`array: ${array}`)
//         console.log(`obj.value: ${obj.value}`)
//         console.log('---------------')
//     }
//
//     return doSome
// }
//
// let refObj = { value: 10 };
//
// let foo = globalTest(2, refObj)
// let bar = globalTest(6, refObj)
//
// bar(2)
// refObj.value++
// foo(4)
// bar(4)

// (function (i) {
//     console.log(i + 10)
// })(8)
//
// function plusArg (i){
//     console.log(i + 10)
// }
//
// plusArg(8)

//
// let login = document.querySelector('.login')
// let btn = document.querySelector('.btn')
//
// const admin = 'Vasia';
//
// function UserException(message) {
//     this.message = message;
//     this.name = 'UserException'
// }
//
// btn.addEventListener('click', () =>{
//     try {
//         if (login.value !== admin && login.value){
//             throw new UserException('Go out')
//         } else if (login.value === ''){
//             throw new UserException('Input is empty')
//
//         } else{
//             console.log('Hello, admin')
//         }
//     } catch (error){
//         console.log(error)
//     }
// })

let arr = [
    {
        role: 'user',
        age: 16
    },
    {
        name: 'Vlad',
        role: 'admin',
        age: 25
    },
    {
        name: 'Nikita',
        role: 'user',

        age: 32
    },
    {
        name: 'Vasya',
        role: 'guest',
        age: 10
    },
    {
        name: 'Jake',
        role: 'user',
        age: 55
    }

]

function RoleError(message){
    this.message = message;
    this.name = 'RoleError'
}

function MissData(message){
    this.message = message;
    this.name = 'MissData'
}

function WrongAge(message){
    this.message = message;
    this.name = 'WrongAge'
}

for (let item of arr) {
    console.log(item.role);
    try {
        if (item.role !== 'guest' && item.role !=='admin' && item.role !=='user') {
            throw new RoleError('Please enter your role')
        }
        // try {
        //     if (item.name || item.age || item.role === undefined) {
        //         throw new MissData("You haven't filled some data")
        //     }
        //
        //     try {
        //         if (item.age < 18) {
        //             throw new WrongAge("You' are too young body")
        //         }
        //     } catch (error) {
        //         console.log(error)
        //     }
        //
        // } catch (error) {
        //     console.log(error)
        // }
    } catch (error) {
        console.log(error)
    }
}
