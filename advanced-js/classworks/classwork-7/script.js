let posts = null
let users = null

function searchUserData(id, users){
    let data = users.filter((el) =>{
        if (el.id === id){
            return {
                name: el.name,
                email: el.email
            }
        }
    })
    return data
}

function createCards(posts, users) {
    let cardsContainer = document.querySelector('.cards');
    posts.map(({title, body, userId}) =>{
        let userData = searchUserData(userId, users)
        let template = `<div class="card">
        <span class="card__title">${title}</span>
        <p class="card__text">
            ${body}
        </p>
        <span>${userData[0].name}</span>
        <span>${userData[0].email}</span>
    </div>`
        cardsContainer.insertAdjacentHTML('beforeend', template)
    })
}

async function getUsers(url) {
    let response = await fetch(url)
    let data = await response.json()
    return data;
}

async function getPosts(url) {
    let response = await fetch(url)
    let data = await response.json()
    return data;
}

// function callAPI(url) {
//     fetch(url)
//         .then((data) => {
//             return data.json()
//         })
//         .then((data) => {
//             console.log(data);
//         })
// }

Promise.all([getUsers('http://jsonplaceholder.typicode.com/users'), getPosts('http://jsonplaceholder.typicode.com/posts')])
    .then((results) =>{
    console.log(results)
    users = results[0];
    posts = results[1];
    createCards(posts, users)
})
.catch((error) =>{
    console.log(error);
})