function callAPI(url) {
  fetch(url)
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      console.log(data);
    });
}

callAPI("http://api.ipify.org/?format=json");
callAPI("http://ip-api.com/json/195.24.146.94");

async function callAPI(url) {
  let response = await fetch(url);
  let data = await response.json();
  return console.log(data);
}

callAPI("http://api.ipify.org/?format=json");
