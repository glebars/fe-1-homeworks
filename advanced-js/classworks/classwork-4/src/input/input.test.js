const inputLogger = require('./input')

describe('Test input logic', () =>{
    test('Check input value for text', ()=>{
        const testData = {
            target: {
                value: 'ashkjshajhsh',
            },
        }
        expect(inputLogger(testData)).toBe('Not a number')
    })
    test('Check input value for short phone', ()=>{
        const testData = {
            target: {
                value: '0674187832',
            },
        }
        expect(inputLogger(testData)).toBe('+380674187832')
    })
})