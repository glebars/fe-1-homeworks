// class Car {
//     constructor(model, color, volume) {
//         this.model = model
//         this.color = color
//         this.volume = volume
//     }
//     startEngine (){
//         console.log('brum brum')
//     }
//     getData (params) {
//         return this[params];
//     }
// }
//
// class Minivan extends Car{
//     constructor(model, color, volume, doors, maxSpeed) {
//         super(model, color, volume);
//         this.doors = doors
//         this.maxSpeed = 130
//     }
// }
//
// let MyCar = new Car('BMW', 'red', 2.0);
// let NewMinivan = new Minivan('Smart', 'White', '1.4', 5)
// MyCar.startEngine()
//
// console.log(MyCar.model);
// console.log(MyCar.color);
// console.log(MyCar.volume);
// console.log(MyCar.getData("model"))
// console.log("------------")
// console.log(NewMinivan.model);
// console.log(NewMinivan.color);
// console.log(NewMinivan.volume);
// console.log(NewMinivan.doors);

// class Worker {
//     constructor(name, surname, rate, days) {
//         this.name = name;
//         this.surname = surname;
//         this.rate = rate;
//         this.days = days;
//     }
//
//     getSalary() {
//         return this.days * this.rate
//     }
// }
//
// let worker = new Worker('Иван', 'Иванов', 10, 31);
//
// console.log(worker.name);
// console.log(worker.surname)
// console.log(worker.rate)
// console.log(worker.days)
// console.log(worker.getSalary())

// class Worker {
//     constructor(name, surname, days, rate) {
//         this._name = name // нижнее подчёркивание делает поле условно-приватным
//         this._surname = surname;
//         this._days = days;
//         this._rate = rate;
//     }
//
// }
//
// let worker = new Worker('Иван', 'Иванов', 31);
//
// console.log(worker._name);
// console.log(worker._surname)
// console.log(worker._days)
// console.log(worker._rate)

// class Employee{
//     constructor(name, age, salary) {
//         this._name = name
//         this.age = age
//         this.salary = salary
//     }
//     get salary(){
//         return this._name
//     }
//     set salary(name){
//         this._name = name
//     }
// }
//
// let employee = new Employee()
//
// class Programmer extends Employee{
//     constructor(name, age, salary, lang) {
//         super(name, age, salary)
//     }
//
//     get salary(){
//         return this._salary * 7
//     }
// }

const HORIZONTAL_CORDS_VALUES = [0, 1, 2];
let pers = null;
let goomba = null

function hidePers() {
    let userRow = document.querySelector(".user-row");

    Array.from(userRow.children).forEach((element) => {
        element.innerHTML = "";
    });
}

function appearPers(cord) {
    let userRow = Array.from(document.querySelector(".user-row").children);

    if (HORIZONTAL_CORDS_VALUES.includes(cord)) {
        userRow[cord].insertAdjacentHTML("afterbegin",' <img src="./mario-img" alt="">');
    }
}

function startGame() {
    hidePers();
    pers = new Mario();
    pers.cords = 1;
    goomba = new Barrier(0, 0)
}

class Mario {
    constructor(cords) {
        this._cords = cords;
    }

    get cords() {
        return this._cords;
    }

    set cords(value) {
        this._cords = value;
        hidePers();
        appearPers(this._cords);
    }
    goLeft() {
        if (this._cords !== 0){
            this._cords = this._cords - 1
            hidePers();
            appearPers(this._cords);
        }
    }
    goRight() {
        if (this._cords !== 2){
            this._cords = this._cords + 1
            hidePers();
            appearPers(this._cords);
        }
    }
}

class Barrier {
    constructor(cordX, cordY) {
        this._cordX = cordX;
        this._cordY = cordY;
    }
    start() {
        document.querySelector(`.r${this._cordX}c${this._cordY}`).insertAdjacentHTML('afterbegin', '<img src="./mario-mushroom.png" alt="">')

    }
}

function persMove(vector) {
    if (vector === 'ArrowLeft') {
        pers.goLeft()
    } else if (vector === 'ArrowRight'){
        pers.goRight()
    }
}



document.addEventListener("DOMContentLoaded", () => {
    startGame();
});

document.addEventListener("keydown", (e) => {
    persMove(e.key)
});

