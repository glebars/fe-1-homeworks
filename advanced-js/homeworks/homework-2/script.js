const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
let correctBooksArr = []
let wrongBookArr = []

function DataAbsence(message){
    this.message = message;
    this.name = 'DataAbsence'
}

function findCorrectItems(arr) {
    for (let item of arr) {
        if ((item.author && item.name && item.price) !== undefined) {
            correctBooksArr.push(item)
        } else {
            wrongBookArr.push(item)
        }
    }
    if (wrongBookArr.length > 0) {
        throw new DataAbsence('Please fill all data')
    }
}

try {
    findCorrectItems(books)
} catch (error) {
    console.error(error)
}


let mainList = document.createElement('ul')
document.body.prepend(mainList)

function listFromMas() {
    correctBooksArr.map((item) => {
        for (let key in item){
            let listItem = document.createElement("li")
            listItem.innerHTML = item[key];
            mainList.append(listItem)
        }
    })
}

listFromMas()