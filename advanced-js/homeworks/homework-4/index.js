let div = document.querySelector(".films");
document.body.prepend(div);


function filmsData(url) {
  fetch(url)
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      return data.results;
    })
    .then((data) => {

      
      data.filter((item) => {
        let { episode_id, title, opening_crawl, characters } = item;


        let allTitles = {title}
        let filmsInfo = { episode_id, title, opening_crawl };

        for (let key in filmsInfo) {
          let listItem = document.createElement("ol");
          listItem.innerHTML = filmsInfo[key];
          div.append(listItem);
          if (key === 'title'){

            let titleEl = listItem
            titleEl.classList.add('title')

    
          }
        }
        
        getAllCharacters(item);
      });
    });
}



function getAllCharacters(char) {
  let titles = document.querySelectorAll('.title')

  let {characters} = char
  let allCharactersArr = characters


  
    for (let key in allCharactersArr) {
      let charLink = allCharactersArr[key];
      fetch(charLink)
      .then((data) =>{
        return data.json();
      })
      .then((data) =>{
        let charName = data.name
        titles.forEach((title) =>{
          title.insertAdjacentHTML("afterend", `<span>${charName}</div>`)
        })
      })
      
    }
}

filmsData("http://swapi.dev/api/films");