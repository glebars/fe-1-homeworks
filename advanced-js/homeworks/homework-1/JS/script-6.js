const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}


const newObj = {
    ...employee,
    age: 50,
    salary: 100.000
}

console.log(newObj);