let div = document.querySelector('.info-container')
document.body.append(div)

async function findAPI(url) {
    let response = await fetch(url)
    let data = await response.json()
    let IP = await fetch(`http://ip-api.com/json/${data.ip}?fields=status,message,continent,country,region,city,district`)
    let trueIP = await IP.json()
    console.log(trueIP);
    let {continent, country, region, city} = trueIP
    let info = {continent, country, region, city}
    console.log(info);

    for (let key in info) {
        let listItem = document.createElement("li")
        listItem.innerHTML = trueIP[key];
        div.append(listItem)
    }
}

let btn = document.querySelector('.button')

btn.addEventListener('click', () => {
    findAPI('http://api.ipify.org/?format=json')
})

// div.append(trueIP.continent, trueIP.country, trueIP.city, trueIP.region)
