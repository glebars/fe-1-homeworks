class Employee{
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name
    }
    set name(name){
        this._name = name
    }
    get age(){
        return this._age
    }
    set age(age){
        this._age = age
    }
    get salary(){
        return this._salary
    }
    set salary(salary){
        this._salary = salary
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang
        this._salary = salary
    }

    get lang(){
        return this._lang
    }

    set lang(lang){
        this._lang = lang
    }

    get salary(){
        return this._salary
    }

    set salary(value){
        this._salary = value
    }


}

let employee = new Employee('Gleb', 16, '500$')
let programmer = new Programmer('Vlad', 25, '3000', 5)

programmer.salary = programmer.salary * 3 + '$'

console.log(employee);
console.log(programmer);