let inp= document.querySelectorAll('input')
let icon = document.getElementsByTagName("i")
let btn = document.querySelector("button")

function switchingTypeOfInp (elem){
    if (elem.previousElementSibling.type === 'password'){
        elem.previousElementSibling.type = 'text'
        elem.classList.add('fa-eye-slash')
        elem.classList.remove('fa-eye')
    } else if (elem.previousElementSibling.type === 'text'){
        elem.previousElementSibling.type = 'password'
        elem.classList.add('fa-eye')
        elem.classList.remove('fa-eye-slash')
    }
}

icon[0].addEventListener("click", () => {
    switchingTypeOfInp(icon[0])
})

icon[1].addEventListener("click", () => {
    switchingTypeOfInp(icon[1])
})

btn.addEventListener('click', (e) =>{
    e.preventDefault()
    if (inp[0].value === inp[1].value && inp[0].value !== ""){
        alert('You are welcome')
    } else {
        let invalidText = document.createElement('span')
        btn.before(invalidText)
        invalidText.innerText = "Нужно ввести одинаковые значения"
        invalidText.classList.add('invalid-text')
    }
    let checkInvalidText = document.querySelectorAll('.invalid-text')
    if (checkInvalidText.length === 2){
        checkInvalidText[1].remove()
    }
})
