let number1 = +prompt("Enter your first number");
let action = prompt("Enter math action");
let number2 = +prompt("Enter your second number");
function showMathResult () {
    switch (action) {
        case "+":
            return number1 + number2;
        case "-":
            return number1 - number2;
        case "*":
            return number1 * number2;
        case "/":
            return number1 / number2;
    }
}

alert(`Your result is ${showMathResult(number1, number2, action)}`);
