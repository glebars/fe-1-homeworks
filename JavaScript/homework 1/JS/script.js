// 1. var отличается от let областью видимости. Так певрое является более глобальным и видна везде, а let только в определенных условиях {}. let отличается от const тем, что его можно изменить, а вторая переменная устойчива.
// 2. Использование var считается плохим тоном, так как эта переменная использовалась в старой версии ES5 и является глобальной в коде.

let name = prompt("Enter your name")
while (name === "" || name === null){
    name = prompt("Enter your name")
}
let age = +prompt("Enter your age?")
while (isNaN(age) || age === 0){
    age = +prompt("Enter your age?")
}
function validateAge(userAge, userName) {
    if (userAge < 18) {
        alert("You are not allowed to visit this website.")
    } else if (18 < age < 22) {
        const logging = confirm("Are you sure you want to continue?")
        if (logging) {
            alert(`Welcome, ${name}`)
        } else {
            alert("You are not allowed to visit this website.")
        }
    }
}
validateAge(age, name)
