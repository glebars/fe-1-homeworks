let currentLink = document.querySelector('link');
let btn = document.querySelector('.changing-theme-button');


// localStorage.removeItem('theme')

function checkThemeOnLoad() {
  let themeOnLoad = localStorage.getItem('theme');
  console.log(themeOnLoad)
    if (themeOnLoad === 'green') {
        currentLink.href = "./css/style-blue.css"
    } else if (themeOnLoad === 'blue') {
        currentLink.href = "./css/main.css"
    
    }
}

function changeTheme() {
  let theme = localStorage.getItem('theme');
  if (theme !== null) {
    if (theme === 'blue') {
        currentLink.href = "./css/main.css"
      localStorage.setItem('theme', 'green');
    } else if (theme === 'green') {
      currentLink.href = "./css/style-blue.css"
      localStorage.setItem('theme', 'blue')
    }
  } else if (theme === null) {
    currentLink.href = "./css/main.css"
    localStorage.setItem('theme', 'green')
  }
}

window.onload = () => {
  checkThemeOnLoad()
}

btn.addEventListener('click', changeTheme);

btn.addEventListener('click', (e) =>{
    
})