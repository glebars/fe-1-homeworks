let buttons = document.querySelectorAll("button")

document.addEventListener("keydown", toggle);

function toggle(e) {
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("active");
    }

    if (e.code === 'Enter') {
        buttons[0].classList.toggle('active')
    } else if (e.code === 'KeyS') {
        buttons[1].classList.toggle('active')
    } else if (e.code === 'KeyE') {
        buttons[2].classList.toggle('active')
    } else if (e.code === 'KeyO') {
        buttons[3].classList.toggle('active')
    } else if (e.code === 'KeyN') {
        buttons[4].classList.toggle('active')
    } else if (e.code === 'KeyL') {
        buttons[5].classList.toggle('active')
    } else if (e.code === 'KeyZ'){
        buttons[6].classList.toggle('active')
    }
}