// // Циклы нужны для того, чтобы повторять какое-то действие несколько раз и при этом записать команду всего лишь один раз.

// let number = +prompt("Enter your number?")
// if (number < 5) {
//     alert("Sorry, no numbers")
// } else {
//     for (let i = number; i > 0; i--) {
//         if (i % 5 === 0 ) {
//             console.log(i);
//         }
//     }
// }




let number = +prompt("Enter your number?")
if (number < 5) {
    alert("Sorry, no numbers")
} else{
    while (number % 1 === 0){
        number = +prompt("Enter your number?")
    }
    for (let i = number; i > 0; i--) {
        if (i % 5 === 0){
            console.log(i);
        }
    }
}
