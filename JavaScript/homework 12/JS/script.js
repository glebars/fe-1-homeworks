let imgs = document.querySelectorAll("img")
let toggleStop = document.createElement('button')
let toggleContinue = document.createElement('button')

toggleStop.classList.add('toggle')
toggleStop.innerText = 'Прекратить'
document.body.prepend(toggleStop)

toggleContinue.classList.add('toggle')
toggleContinue.innerText = 'Возобновить показ'
toggleStop.after(toggleContinue)

function interval (){
    let index = null
    imgs.forEach((el, i ) =>{
        if(el.classList.contains('active')){
            index = i
        }
    })
    if (index !== imgs.length - 1){
        imgs[index].classList.remove('active')
        imgs[index + 1].classList.add('active')
    } else {
        imgs[index].classList.remove('active')
        imgs[0].classList.add('active')
    }
}

let timerId = setInterval(interval,10000)

toggleStop.addEventListener('click', () =>{
    clearInterval(timerId)
})

toggleContinue.addEventListener('click', () =>{
    setInterval(interval, 10000)
})
