$(document).ready(function() {
    function smoothScroll() {
        $('a[href^="#"]').click(function (e) {
            let target = $(this).attr('href')
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1000)
        })
    }

    smoothScroll()

    $(window).scroll(function () {
        if ($(window).scrollTop() >= 460) {
            $('.hidden-btn').addClass('btn-active')
        } else {
            $('.hidden-btn').removeClass('btn-active')

        }
    })

    $('.hidden-btn').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 400);
    })

    $(".slide-toggle").click(function () {
        $(".hot-news").slideToggle("slow");
    });
});
