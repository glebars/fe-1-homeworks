function createNewUser() {
    let firstName = prompt("Введите имя")
    let lastName = prompt("Введите фамилию")
    let birthday = prompt("Введите дату рождения в формате дд.мм.гггг")
    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge(){
            let getBirthDay = +this.birthday.slice(0, 2);
            let getBirthMonth = +this.birthday.slice(3, 5);
            let getBirthYear = +this.birthday.slice(6, 10);
            let currentData = new Date();
            let currentDay = currentData.getDate();
            let currentMonth = currentData.getMonth() + 1;
            let currentYear = currentData.getFullYear();
            if (currentMonth > getBirthMonth){
                return (currentYear - getBirthYear)
            }else if (currentMonth <= getBirthMonth){
                if (currentDay >= getBirthDay){
                    return (currentYear - getBirthYear)
                }else{
                    return (currentYear - getBirthYear - 1)
                }
            }
        },
        getPassword(){
            return firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.slice(6, 10);
        }
    }
    return newUser
}

let functionObj = createNewUser()
console.log(`${functionObj.getLogin()} ${functionObj.getAge()} ${functionObj.getPassword()}`)
