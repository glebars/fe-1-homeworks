let inp = document.querySelector("input")
inp.classList.add('price-input');
let divWithInput= document.querySelector("div")
divWithInput.classList.add("input-box")
let divWithPrice = document.createElement('div')
let invalidDiv =  document.body.children[1]

inp.addEventListener("blur", () =>{
    divWithPrice.innerHTML = `<div class="price-box"><span class="price-text">Текущая цена: ${inp.value}</span> <button class="price-button">&times;</button></div>`
    document.body.prepend(divWithPrice)
    inp.style.color = "green"
    let btn = document.querySelector('button')
    btn.onclick = () => {
        btn.remove()
        document.body.children[0].remove()
        inp.value = null
        inp.style.color = "black"
    }
})

inp.addEventListener('blur', () =>{
    if (inp.value.includes('-')){
        invalidDiv.innerHTML = 'Please enter correct price'
        inp.classList.add('invalid-inp')
        invalidDiv.classList.add('invalid-div')
        divWithPrice.innerHTML = ""
        inp.style.color = "red"
    }
})

inp.addEventListener("focus", () =>{
    if (inp.value.includes('-')) {
        inp.classList.remove('invalid-inp')
        invalidDiv.classList.remove('invalid-div')
        invalidDiv.innerHTML = ""
        inp.style.color = "black"
    }
})